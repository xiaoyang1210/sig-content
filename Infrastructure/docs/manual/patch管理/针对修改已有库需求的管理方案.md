# 问题

目前已有sig提交代码时发现以下问题：

某些sig的任务需要对openharmony已有代码仓做修改，而在sig组织下建仓，不会新建主线已有的仓库，只能通过repo来拉取对应的代码仓，但是其中并不包含sig已经做出的修改，这就导致社区门禁或者社区参与者拉取的该任务代码，因为不包含对已有仓的修改，而编译不过

# 解决方案

1. 对于这类在某个sig下需要修改openharmony 已有仓的需求，无论只修改一次还是需要多次维护，我们推荐都放在负责单位的私仓下进行维护

2. 其中，对于只需要修改一次的仓库，例如某个和编译配置相关的库，建议自行维护一个patch库，对sig下的manifest仓的xml文件做以下修改：

   * 给出相应patch的下载路径，以及在代码目录下的存放路径。
   * patch和修改库的对应关系文件，具体实现可参考此[链接](./patch_info.txt)。

   manifest.xml  添加patch路径样例：

   ```shell
   <project name="oh-rpi3b-projectpatch" path="projectpatch" revision="master" upstream="master" dest-branch="master" remote="xfan1024"/>
   ```

   

3. 其次，对于需要多次维护的仓库，例如完善openharmony的某个特征库，建议从主仓forkf该仓库到私仓来进行维护，在sig下的manifest仓的xml文件中给出相应库的下载路径即可，将来有孵化毕业需求，只需向主仓提PR合入即可。

   



