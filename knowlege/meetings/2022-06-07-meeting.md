# Jun 7, 2022 at 15:00pm GMT+8

## Agenda
+ 遗留问题汇总
+ 深开鸿开发样例上官网申请
+ 深开鸿知识赋能直播申报
+ 成长计划学生挑战赛开发板部分采购需求变更

## Attendees
- [zhanglunet](https://gitee.com/zhanglunet)
- [Cruise2019](https://gitee.com/Cruise2019)
- [madixin](https://gitee.com/madixin)
- [kenio_zhang](https://gitee.com/kenio_zhang)
- [深开鸿-guoyuefeng](guoyuefeng@kaihongdigi.com)
- [成长计划挑战赛-黄吉城]()

## Notes

#### 议题一、遗留问题汇总（张前福）

**进展**
知识体系当前的共建流程梳理与汇总，基本流程如下：
+ [共建开发样例流程流程参考](https://docs.qq.com/slide/DUWtMdWVVc01IVkV6)
+ [共建知识赋能课程流程参考](https://docs.qq.com/slide/DUWFjTFdpVEpldWZF)
+ [共建学习路径流程](https://docs.qq.com/slide/DUURFUE5Mc0VrQWJB)
+ [共建知识体系文章流程](https://docs.qq.com/slide/DUWFGTGhTUkhJaktB)

**结论**
+ 后续开发样例/赋能课程/学习路径/深度文章都按照梳理的流程和模板进行。

#### 议题二、深开鸿开发样例上官网（郭岳峰）

**进展**
  深开鸿提供了两个样例：
+ 录音变声样例：该样例是装在手机端的应用，适用于OpenHarmony3.1_release版本，使用AudioCapturer提供的JS接口对音频进行采集，并进行变声处理。
+ 视频裁剪样例：在OpenHarmony系统整个框架中有很多子系统，其中多媒体子系统是OpenHarmony比较重要的一个子系统，OpenHarmony中集成了ffmpeg的第三方库，多媒体的很多实现都与ffmpeg库有关，媒体文件的处理有很多应用场景，比如音视频的裁剪，音视频的分离等等。有些功能OpenHarmony多媒体子系统没有提供给外部相应的接口，我们可以通过NAPI的机制自己实现一套接口，提供给应用层去调用。

**结论**

 会议同意深开鸿两个样例上官网的申请。

#### 议题三、深开鸿知识赋能直播申报（郭岳峰）

**进展**

深开鸿申请知识赋能直播，主要涉及的Ability子系统，Sensor子系统，短距离通信子系统，ACE子系统。

**结论**
直播课程倾向于简单直接，实例化；录播课程倾向于原理解读。可以从申报内容中选取两个左右的课题，进行拆分，进行直播，其他模块可以后续通过录播的方式进行。后续会联合知识体系组和运营组确认直播专题并细化。

#### 议题四、成长计划学生挑战书开发板部分采购需求变更（黄吉城）

**进展**
成长计划学生挑战赛开发板采购变更，不适配OpenHarmony3.1release及以上版本的开发板进行适配更换，更换的开发板需有采购渠道与相对开发样例，若无替换的开发板，则取消采购。涉及软通、深开鸿、拓维、传智等开发板，原因主要包含不支持3.1 release及其以上、未上架、未有样例支撑等原因，需要和厂家进行沟通确认，及时上传相关样例。

## Action items
无
